package ar.com.grupoesfera.registrarme.adaptadores;

/**
 * Created by diego on 9/25/18.
 */
public interface AdaptadorParaRegistrarme {

    String CLAVE_VALIDA = "masdediezcaracteres";

    void noExisteUsuario(String usuario);

    void agregarUsuario(String usuario, String clave);

    void registrarme(String usuario, String clave);

    void usuarioSeCrea();

    void usuarioNoEstaRegistrado();

    void muestraMensaje(String mensaje);

    void meEncuentroEn(String vista);

    void borrarRepositorio();

    void registrarseClave(String clave);
}

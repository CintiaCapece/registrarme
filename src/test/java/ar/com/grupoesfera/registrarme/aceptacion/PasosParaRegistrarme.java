package ar.com.grupoesfera.registrarme.aceptacion;

import ar.com.grupoesfera.registrarme.adaptadores.AdaptadorParaRegistrarme;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PasosParaRegistrarme implements AdaptadorParaRegistrarme {

    private final AdaptadorParaRegistrarme adaptador;

    public PasosParaRegistrarme(AdaptadorParaRegistrarme adaptador ){
        this.adaptador = adaptador;
    }

    @Before
    public void borrarRepositorio() {
        this.adaptador.borrarRepositorio();
    }

    @Given("que no existe el usuario {}")
    public void noExisteUsuario(String usuario){
        adaptador.noExisteUsuario(usuario);
    }

    @Given("que ya existe el usuario {} con la clave {}")
    public void agregarUsuario(String usuario, String clave){
        adaptador.agregarUsuario(usuario,clave);
    }

    @When("intenta registrarse con clave {}")
    public void registrarseClave(String clave){
        adaptador.registrarseClave(clave);
    } 

    @When("intento registrarme como {} con la clave {}")
    public void registrarme(String usuario, String clave){
        adaptador.registrarme(usuario,clave);
    }

    @Then("el usuario se crea")
    public void usuarioSeCrea(){ adaptador.usuarioSeCrea();     }

    @Then("el usuario no está registrado")
    public void usuarioNoEstaRegistrado(){ adaptador.usuarioNoEstaRegistrado();     }

    @Then("muestra el mensaje '{}'")
    public void muestraMensaje(String mensaje){
        adaptador.muestraMensaje(mensaje);
    }

    @And("me encuentro en {}")
    public void meEncuentroEn(String vista){
        adaptador.meEncuentroEn(vista);
    }
}

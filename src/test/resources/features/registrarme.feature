# language: en

Feature: Registrarme y darme de alta en el sitio

  Scenario: El usuario se registra y queda listo para ingresar
    Given que no existe el usuario maria@maria.com
    When intento registrarme como maria@maria.com con la clave masdediezcaracteres
    Then me encuentro en login

  Scenario: El usuario no logra registrarse y puede reintentar
    Given que ya existe el usuario pedro@pedro.com con la clave masdediezcaracteres
    When intento registrarme como pedro@pedro.com con la clave masdediezcaracteres
    Then me encuentro en nuevo-usuario
    And muestra el mensaje 'El usuario ya existe'

  Rule: Solo puede registrarse si el usuario no existe

    Scenario: Si el usuario no existe en el sitio, se da de alta
      Given que no existe el usuario maria@maria.com
      When intento registrarme como maria@maria.com con la clave masdediezcaracteres
      Then el usuario se crea

    Scenario: Si el usuario ya existe en el sitio, no se da de alta
      Given que ya existe el usuario pedro@pedro.com con la clave masdediezcaracteres
      When intento registrarme como pedro@pedro.com con la clave masdediezcaracteres
      Then muestra el mensaje 'El usuario ya existe'

  Rule: El usuario debe ser un email con formato válido

    Scenario: Si el formato de usuario es incorrecto no se da de alta
      Given que no existe el usuario pedro.com
      When intento registrarme como pedro.com con la clave masdediezcaracteres
      Then el usuario no está registrado
      And muestra el mensaje 'El formato del usuario no es una direccion de email válida'

  Rule: El usuario debe registrarse con una clave de longitud válida

    Scenario: Si la longitud de la clave es de más de diez caracteres, el usuario se registra
     Given que no existe el usuario maria@maria.com 
     When intento registrarme como maria@maria.com con la clave masdediezcaracteres
     Then el usuario se crea
        
    Scenario: Si la longitud de la clave es de menos de diez caracteres, el usuario no se registra
      Given que no existe el usuario maria@maria.com
      When intento registrarme como maria@maria.com con la clave ic2022
      Then el usuario no está registrado
      And muestra el mensaje 'La clave debe tener como minimo diez caracteres'